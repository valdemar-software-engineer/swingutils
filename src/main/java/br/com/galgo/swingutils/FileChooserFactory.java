/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.swingutils;

import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * Fábrica de FileChoosers
 *
 * @author valdemar.arantes
 */
public class FileChooserFactory {

    /**
     * Cria um JFileChooser para abrir arquivos XML
     *
     * @param currentDir Diretório que inicial da caixa de diálogo
     * @return
     */
    public static JFileChooser newOpenXML(File currentDir) {

        // Abre caixa para selecionar arquivo XML
        JFileChooser selecionarXML = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Arquivos (*.xml)", "xml");
        selecionarXML.setDialogTitle("Escolha o nome do arquivo");
        selecionarXML.setApproveButtonText("Abrir");
        selecionarXML.setAcceptAllFileFilterUsed(false);
        selecionarXML.setMultiSelectionEnabled(false);
        selecionarXML.setFileFilter(filter);

        // Recuperando a pasta do último acesso efetuado
        if (currentDir != null) {
            selecionarXML.setCurrentDirectory(currentDir);
        }

        return selecionarXML;
    }

    private FileChooserFactory() {
    }

    /**
     * Cria um JFileChooser para salvar arquivos XML
     *
     * @param currentDir Diretório que inicial da caixa de diálogo
     * @return
     */
    public static JFileChooser newSaveXML(File currentDir) {

        // Abre caixa para selecionar arquivo XML
        JFileChooser selecionarXML = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Arquivos (*.xml)", "xml");
        selecionarXML.setDialogTitle("Escolha o nome do arquivo");
        selecionarXML.setApproveButtonText("Salvar");
        selecionarXML.setAcceptAllFileFilterUsed(false);
        selecionarXML.setMultiSelectionEnabled(false);
        selecionarXML.setFileFilter(filter);

        // Recuperando a pasta do último acesso efetuado
        if (currentDir != null) {
            selecionarXML.setCurrentDirectory(currentDir);
        }

        return selecionarXML;
    }

    /**
     * Cria um JFileChooser para salvar arquivos de extensão parametrizável
     *
     * @param currentDir Diretório que inicial da caixa de diálogo
     * @param extension Extensão do arquivo a ser salvo
     * @return
     */
    public static JFileChooser newSaveTypedFile(File currentDir, String extension) {

        // Abre caixa para selecionar arquivo XML
        JFileChooser selecionar = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                "Arquivos (*." + extension + ")", extension);
        selecionar.setDialogTitle("Escolha o nome do arquivo");
        selecionar.setApproveButtonText("Salvar");
        selecionar.setAcceptAllFileFilterUsed(false);
        selecionar.setMultiSelectionEnabled(false);
        selecionar.setFileFilter(filter);

        // Recuperando a pasta do último acesso efetuado
        if (currentDir != null) {
            selecionar.setCurrentDirectory(currentDir);
        }

        return selecionar;
    }

    /**
     * Cria um JFileChooser para selecionar uma pasta onde serão salvos os arquivos.
     *
     * @param currentDir Diretório que inicial da caixa de diálogo
     * @param title
     * @param buttonText
     * @return
     */
    public static JFileChooser newSaveToDir(File currentDir, String title, String buttonText) {

        // Abre caixa para selecionar arquivo XML
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle(title);
        fileChooser.setApproveButtonText(buttonText);
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);

        // Recuperando a pasta do último acesso efetuado
        if (currentDir != null) {
            fileChooser.setCurrentDirectory(currentDir);
        }

        return fileChooser;
    }

}
