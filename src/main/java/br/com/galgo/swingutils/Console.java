/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.galgo.swingutils;

import java.io.IOException;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import java.io.PrintStream;
import javax.swing.JTextArea;

/**
 *
 * Classe que redireciona as saídas de System.out e System.err para um componente JText do Swing.
 *
 * @author valdemar.arantes
 */
public class Console {

    private static JTextArea txtOut;
    private static boolean quit = false;
    private static final PipedInputStream pin = new PipedInputStream();

    private static enum STATUS {

        NOT_STARTED, STARTED
    };
    private static STATUS status = STATUS.NOT_STARTED;

    private Console() {
    }

    public static void redir(JTextArea txtOut) {
        try {

            if (status.equals(STATUS.STARTED)) {
                return;
            }

            System.out.println("Redirecionando System.out e System.err para o componente " + txtOut);
            status = STATUS.STARTED;

            Console.txtOut = txtOut;
            PipedOutputStream pout = new PipedOutputStream(Console.pin);
            System.setOut(new PrintStream(pout, true));
            System.setErr(new PrintStream(pout, true));
        } catch (Exception e) {
            txtOut.append("Erro ao redirecionar STDOUT para este componente\n" + e.getMessage());
        }

        Thread reader = new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                try {
                    while (true) {
                        try {
                            this.wait(100);
                        } catch (InterruptedException ie) {
                        }
                        if (pin.available() != 0) {
                            String input = Console.readLine(pin);
                            Console.txtOut.append(input);
                            Console.txtOut.setCaretPosition(Console.txtOut.getDocument().getLength()
                                    - 1);
                        }
                    }
                } catch (Exception e) {
                    Console.txtOut.append("\nConsole reports an Internal error.");
                    Console.txtOut.append("The error is: " + e);
                }
            }
        });

        reader.setDaemon(true);
        reader.start();
    }

    public static synchronized String readLine(PipedInputStream in) throws IOException {
        String input = "";
        do {
            int available = in.available();
            if (available == 0) {
                break;
            }
            byte b[] = new byte[available];
            in.read(b);
            input += new String(b, 0, b.length);
        } while (!input.endsWith("\n") && !input.endsWith("\r\n") && !quit);
        return input;
    }
}
